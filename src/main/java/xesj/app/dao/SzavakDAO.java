package xesj.app.dao;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * SZAVAK dao 
 */
@Repository
public class SzavakDAO {
 
  @Autowired NamedParameterJdbcTemplate jdbc;

  /**
   * Tábla létrehozása
   */
  public void tablaLetrehozasa() {
    
    jdbc.update("DROP TABLE IF EXISTS szavak", (Map)null);
    jdbc.update("CREATE TABLE szavak(szo varchar(100) primary key)", (Map)null);
    
  }
  
  /**
   * Szó létrehozása
   */
  public void letrehozas(String szo) {

    jdbc.update("INSERT INTO szavak(szo) VALUES(:szo)", new MapSqlParameterSource("szo", szo));

  }  

  /**
   * Összes szó lekérdezése
   */
  public List<String> osszesLekerdezese() {
    
    return jdbc.queryForList("SELECT szo FROM szavak ORDER BY szo", (Map)null, String.class);

  }
  
  // ====
}
