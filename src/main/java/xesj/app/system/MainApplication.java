package xesj.app.system;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
@Getter
public class MainApplication {

  /**
   * Application context nem spring bean-ek számára
   */
  @Getter
  @Setter
  private static ApplicationContext context;

  // ====
}
