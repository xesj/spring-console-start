package xesj.app.system;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import xesj.app.dao.SzavakDAO;

/**
 * Run
 */
@SpringBootApplication(scanBasePackages = "xesj.app")
public class Run implements ApplicationRunner {
  
  @Autowired ApplicationContext applicationContext;
  @Autowired Property property;
  @Autowired SzavakDAO szavakDAO;
  
  /**
   * Alkalmazás indítás
   */
  @Override
  public void run(ApplicationArguments applicationArguments) {
    
    // Application context beállítása statikus változóba
    MainApplication.setContext(applicationContext);

    // Tábla létrehozása
    szavakDAO.tablaLetrehozasa();
    
    // Tábla feltöltése
    String[] szavak = {"tarka boci", "hóbagoly", "jegesmedve"};
    for (String szo: szavak) szavakDAO.letrehozas(szo);
    
    // Program argumentumok
    System.out.println("PROGRAM ARGUMENTUMOK:");
    for (String arg: applicationArguments.getSourceArgs()) {
      System.out.println("  -->  " + arg);
    }
    
    // Az adatbázisban tárolt szavak kiírása
    System.out.println("ADATBÁZISBAN TÁROLT SZAVAK:");
    for (String szo: szavakDAO.osszesLekerdezese()) {
      System.out.println("  -->  " + szo);
    }

    // Környezeti beállítás kiírása
    System.out.println("APP.NAME KÖRNYEZETI BEÁLLÍTÁS:");
    System.out.println("  -->  " + property.getName());
    
  }

  /**
   * Spring Boot inicializálás
   */
  public static void main(String[] args) throws Exception {
    
    SpringApplication.run(Run.class, args);

  }
  
  // ====
}
