package xesj.app.system;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * Main configuration
 */
@Configuration
public class MainConfiguration {
  
  /**
   * Datasource a H2 memory adatbázishoz
   */
  @Bean
  @ConfigurationProperties("app.db")
  public DataSource dataSource() {

    return DataSourceBuilder.create().build();

  }
  
  /**
   * NamedParameterJdbcTemplate a H2 memory adatbázishoz
   */
  @Bean
  public NamedParameterJdbcTemplate jdbc(@Autowired DataSource datasource) {
    
    return new NamedParameterJdbcTemplate(datasource);
    
  }

  // ====
}
