package xesj.app.system;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

/**
 * Alkalmazás jellemzők
 */
@Service
@ConfigurationProperties("app")
@Validated
@Getter
@Setter
public class Property {
  
  @NotNull
  private String name;

}
